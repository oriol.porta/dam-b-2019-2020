package bloc2;

//Ex 9. Algorisme que llegeix dos números enters positius i ens dóna el producte mitjançant sumes successives.

import java.util.Scanner;  //Classe de Java que ens permet llegir el contingut d'entrada de dades

public class bloc2Ex9 {
	
	public static void main (String [] args) {
		Scanner reader = new Scanner(System.in);  //Creem un objecte que treballa sobre la consola (System.in)
			
		int num1 = 0, num2 = 0; 
		int producte = 0;
		int cont = 0;
		boolean fi = false;
		
		
		while (!fi)
		{
			System.out.print("Introdueix un número: ");
			try {
				num1 = reader.nextInt();  
				if (num1 > 0) fi = true;
				else  System.out.println("Atenció! introdueix un número positiu");
			} catch (Exception e){
				System.out.println("Atenció! Únicament es permet insertar números enters. ");
				reader.nextLine();    //per netejar el buffer d'entrada del \n que ha quedat darrere de llegir un número
			}	
		}
		
		fi = false;
		while (!fi)
		{
			System.out.print("Introdueix un número: ");
			try {
				num2 = reader.nextInt();  
				if (num2 > 0) fi = true;
				else  System.out.println("Atenció! introdueix un número positiu");
			} catch (Exception e){
				System.out.println("Atenció! Únicament es permet insertar números enters. ");
				reader.nextLine();    //per netejar el buffer d'entrada del \n que ha quedat darrere de llegir un número
			}	
		}
		
		//Els dos números llegits són correctes
		
		cont = num1;
		while (cont > 0) {
				producte = producte + num2;
				cont --;
		}
		
			
		System.out.println("El producte " + num1 + " * " + num2 + " = " + producte);
		
				
	}  //Fi dael maain
}

