package bloc2;

import java.util.Scanner;

public class Exercici6 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int num, pos, neg, zero, cont;
		cont = 0;
		pos = 0;
		neg = 0;
		zero = 0;

		while (cont < 10) {

			System.out.print("Pon un n�mero: ");
			num = sc.nextInt();
			if (num > 0) {
				pos++;
			} else if (num == 0) {
				zero++;
			} else if (num < 0) {
				neg++;
			}
			cont++;

		}
		System.out.print("Hay " + pos + " positivos, " + neg + " negativos y " + zero + " zeros");
		sc.close();
	}

}
