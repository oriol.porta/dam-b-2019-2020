package bloc2;

//Ex 7. Algorisme que llegeix una seqüència de números enters fins a llegir un zero. Llavors ens diu quin és el número més gran.


import java.util.Scanner;  //Classe de Java que ens permet llegir el contingut d'entrada de dades

public class bloc2Ex7 {
	
	public static void main (String [] args) {
		Scanner reader = new Scanner(System.in);  //Creem un objecte que treballa sobre la consola (System.in)
			
		int cont = 0, num = 0;
		int gran = Integer.MIN_VALUE;
		boolean fi = false;
		
		
		while (!fi)
		{
			System.out.print("Introdueix un número: ");
			try {
				num = reader.nextInt();  
				if (num == 0) fi = true;
				else {
					if (num > gran) gran = num;
				}
			} catch (Exception e){
				System.out.println("Atenció! Únicament es permet insertar números enters. ");
				reader.nextLine();    //per netejar el buffer d'entrada del \n que ha quedat darrere de llegir un número
			}	
		}
		
		System.out.println("El número més gran és: " + gran);
		
				
	}  //Fi dael maain
}

