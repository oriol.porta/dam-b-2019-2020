package bloc2;

//Ex 1. Algorisme que llegeix 10 n�meros enters i ens diu quants d'ells s�n positius.



import java.util.Scanner;  //Classe de Java que ens permet llegir el contingut d'entrada de dades

public class bloc2Ex1 {
	
	public static void main (String [] args) {
		Scanner reader = new Scanner(System.in);  //Creem un objecte que treballa sobre la consola (System.in)
		//java.util.Date fechaActual = new java.util.Date(); //Fecha actual del sistema
		
		int num = 0;
		int positius = 0;
		int cont = 0;	
		
		//Obtenir n�meros i tractar-los amb un bocle while
		
		while (cont < 10)
		{
			System.out.print("Introdueix un n�mero: ");
			try {
				num = reader.nextInt();  
				cont++;
				if (num > 0) positius++;
			} catch (Exception e){
				System.out.println("Atenci�! �nicament es permet insertar n�meros enters. ");
				reader.nextLine();    //per netejar el buffer d'entrada del \n que ha quedat darrere de llegir un número
			}	
		}
		
		System.out.print("El total de n�meros positus introduits �s: " + positius);
		
	}  //Fi dael maain
}

