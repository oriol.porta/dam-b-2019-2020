package bloc2;

//Ex 16. Algorisme que llegeix una seqüència de 
//lletres (que formen una frase) fins arribar al 
//punt. La sortida ens diu el número de vegades que 
//llegeix la síl·laba LA (tant majúscula com a minúscula).

import java.util.Scanner;  //Classe de Java que ens permet llegir el contingut d'entrada de dades

public class bloc2Ex16 {
	
	public static void main (String [] args) {
		Scanner reader = new Scanner(System.in);  //Creem un objecte que treballa sobre la consola (System.in)
		
		//char charAtAnyPos= input.charAt(pos);  // in pos you enter that index from where you want to get the char from	
		
		char lletra;
		int contador = 0;
		int pos;
	

		System.out.println("Escriu una frase acabada en punt ");
		
		String input = reader.nextLine();  // you can also write key.nextLine to take a String with spaces also
										//Llegeix la frase sencera i es guarda en input. nextLine para que lea tambien con espacios.
		
		
		lletra = input.charAt(0);		//Llegeix un caracter de teclat
		
		contador = 0;
		pos = 0;
		while (lletra != '.')
		{
			if (lletra == 'l' || lletra == 'L' )
			{
				pos++;
				lletra = input.charAt(pos);
				
				if (lletra == 'a' || lletra == 'A')
				{
					contador=contador+1;
				}
			}
		   else {
			   pos++;
			   lletra = input.charAt(pos);
		   }

		}
		System.out.println("Tienes " + contador + " silabas formadas por la/LA");
		

	}  //Fi dael maain
}

