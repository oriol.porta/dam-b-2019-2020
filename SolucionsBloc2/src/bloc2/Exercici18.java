package bloc2;

import java.util.*;

/* Exercici18: Algorisme que llegeix una seq��ncia de lletres 
 * (que formen una frase) fins arribar al punt. 
 * La sortida ens diu quantes paraules cont� la frase introdu�da.
 * Autor: Oriol Porta Sobrino
 */

public class Exercici18 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc= new Scanner (System.in);
		
		char letra1, letra2;
		String frase;
		int palabras=0, pos=0;;
		boolean cen=true;
		
		System.out.print("Introdueix una frase: ");
		frase=sc.nextLine();
		frase.toLowerCase();
		
		while (cen) {
			
			letra1= frase.charAt(pos);
			
			if (letra1=='.') {
				
				cen=false;
				
			} else if (letra1>='a' && letra1<='z' || 
					letra1>='A' && letra1<='Z') {
				
				letra2=frase.charAt(pos+1);
				
				if (letra2==' ' || letra2=='.') {
					
					palabras++;
					
				}
				
			}
		
			pos++;
			
		}
		
		System.out.println("La frase tiene " + palabras + " palabras.");
		
		sc.close();
		
	}

}
