package bloc2;

import java.util.Scanner;

public class Exercici12V2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner reader = new Scanner(System.in);  //Creem un objecte que treballa sobre la consola (System.in)
		
		int num = 0, major, petit, contador;
		float mitjana;
		boolean fi = false;
		
		
		major = Integer.MIN_VALUE;
		petit = Integer.MAX_VALUE;
		mitjana = 0f;
		contador = 0;

		while (contador < 10)
		{
			System.out.print("Introdueix un número: ");
			try {
				num = reader.nextInt();  
				mitjana= num + mitjana;

				if (num > major) {
					major = num;
				}
				if (num <= petit) {
					petit = num;
				}
				contador = contador + 1;
			} catch (Exception e){
				System.out.println("Atenció! Únicament es permet insertar números enters. ");
				reader.nextLine();    //per netejar el buffer d'entrada del \n que ha quedat darrere de llegir un número
			}	
		}
		mitjana=mitjana/10.0f;
		System.out.println("El numero mes gran es " + major + " el mes petit es " + petit + " i la mitjana aritmetica es " + mitjana);

	}

}
