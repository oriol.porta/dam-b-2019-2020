package bloc2;

import java.util.Scanner;

/*Institut Sabadell
 *CFGS DAM M03 UF1
 *Autor: Isaac Garc�a
 *Bloc 2 Activitat 4
 *4.17.	Algorisme que llegeix una seq��ncia de frases (un vers). El programa acaba quan li diem que no volem introduir m�s frases. 
 *		La sortida ens diu per a cada frase quants car�cters t�. En acabar ens diu quantes frases hem introdu�t.
 */

public class Activitat4_017 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int cont=0;
		int i=0;
		int quiz=0;
		boolean mesFrases=true;
		boolean respostaCorrecta=true;
		System.out.println("Escriu una seq�encia de frases i et dir� quantes frases has introdu�t i de quants car�cters");
		
		
		while (mesFrases)
		{
			System.out.println("Escriu una frase:");
			String frase = sc.nextLine(); 
			cont = frase.length();
			i++;
			System.out.println("Aquesta frase t� "+cont +" car�cters (portes " +i +" frases)" );
			respostaCorrecta=true;
		
			while (respostaCorrecta) 
			{
				try 
				{
					System.out.print("Vols escriure m�s frases? (1)S� (2)No: ");
					sc.nextLine();
					quiz = sc.nextInt();
					if (quiz !=1 && quiz !=2) 
					{
						System.out.println("Nom�s pots posar 1 o 2");
					} 
					else if (quiz == 2)
					{
						mesFrases=false;
						respostaCorrecta=false;
					}
					else
					{
						respostaCorrecta=false;
					}		
				} catch (Exception e){
					System.out.println("Nom�s pots posar n�meros enters");
					sc.nextLine();    //per netejar el buffer d'entrada del \n que ha quedat darrere de llegir un n�mero.
					//fins aqui			
				}		
			}
		}	
	
		sc.close();
		System.out.println("Has introduit "+i +" frases");
	}

}
