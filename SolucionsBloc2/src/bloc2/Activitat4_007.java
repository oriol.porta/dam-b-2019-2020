package bloc2;

import java.util.Scanner;

/*Institut Sabadell
 *CFGS DAM M03 UF1
 *Autor: Isaac Garc�a
 *Bloc 2 Exercici 4.1
 *4.7.	Algorisme que llegeix una seq��ncia de n�meros
 enters fins a llegir un zero. Llavors ens diu quin �s
 el n�mero m�s gran.
 */

public class Activitat4_007 {
	public static final int EXIT = 0;
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int i=1;
		int num=0;
		int max=0;
		
		
		System.out.println("Introdueix tots els n�meros que vulguis, quan hagis acabat posa un " +EXIT +" i et dir� quin �s el m�s gran:");
		while (i!=EXIT) {
			//comprovaci� del tipus de dada introdu�da
			try {
				num = sc.nextInt();
				if (num == EXIT) {
					i=0;
				} else if (num >= max) {
					max=num;
				}
			} catch (Exception e){
				System.out.println("Atenci�! Unicament es permet insertar n�meros enters. ");
				sc.nextLine();    //per netejar el buffer d'entrada del \n que ha quedat darrere de llegir un n�mero
			//fins aqui	
			} 
			
		} System.out.println("El n�mero m�s gran que has posat �s: "+ max);
		sc.close();
	}
}
