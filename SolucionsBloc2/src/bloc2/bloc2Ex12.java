package bloc2;

//Ex 12. Algorisme que llegeix una seqüència de 10
// números per teclat i en acabar ens diu el més
// gran, el més petit i la mitjana aritmètica de tots ells.

import java.util.Scanner;  //Classe de Java que ens permet llegir el contingut d'entrada de dades

public class bloc2Ex12 {
	
	public static void main (String [] args) {
		Scanner reader = new Scanner(System.in);  //Creem un objecte que treballa sobre la consola (System.in)
			
		int num = 0, major, petit, contador;
		float mitjana;
		boolean fi = false;
		
		
		while (!fi)
		{
			System.out.print("Introdueix un número: ");
			try {
				num = reader.nextInt();  
				fi = true;
			} catch (Exception e){
				System.out.println("Atenció! Únicament es permet insertar números enters. ");
				reader.nextLine();    //per netejar el buffer d'entrada del \n que ha quedat darrere de llegir un número
			}	
		}
		
		major = num;
		petit = num;
		mitjana = (float)num;
		contador = 1;

		while (contador < 10)
		{
			System.out.print("Introdueix un número: ");
			try {
				num = reader.nextInt();  
				mitjana= num + mitjana;

				if (num > major) {
					major = num;
				}
				if (num <= petit) {
					petit = num;
				}
				contador = contador + 1;
			} catch (Exception e){
				System.out.println("Atenció! Únicament es permet insertar números enters. ");
				reader.nextLine();    //per netejar el buffer d'entrada del \n que ha quedat darrere de llegir un número
			}	
		}
		mitjana=mitjana/10.0f;
		System.out.println("El numero mes gran es " + major + " el mes petit es " + petit + " i la mitjana aritmetica es " + mitjana);

				
	}  //Fi dael maain
}

