package bloc2;

//Ex 15. Millora de l’exercici anterior per tal que
// ens digui el número de vegades 
//que llegeix cada vocal.

import java.util.Scanner;  //Classe de Java que ens permet llegir el contingut d'entrada de dades

public class bloc2Ex15 {
	
	public static void main (String [] args) {
		Scanner reader = new Scanner(System.in);  //Creem un objecte que treballa sobre la consola (System.in)
		
		
		//char charAtAnyPos= input.charAt(pos);  // in pos you enter that index from where you want to get the char from	
		
		char lletra;
		int conta=0, conte=0, conti=0, conto=0, contu=0;
		int pos = 0;
	

		System.out.println("Escriu una frase acabada en punt ");
		
		String input = reader.nextLine();  // you can also write key.nextLine to take a String with spaces also
										//Llegeix la frase sencera i es guarda en input. nextLine para que lea tambien con espacios.
		
		
		lletra = input.charAt(pos);		//Llegeix un caracter de teclat
		
		while (lletra != '.')
		{
			switch (lletra) {
				case 'a':
				case 'A': conta = conta + 1;
					      break;
				case 'e':
				case 'E': conte = conte + 1;
					      break;
				case 'i':
				case 'I': conti = conti + 1;
					      break;
				case 'o':
				case 'O': conto = conto + 1;
					      break;
				case 'u':
				case 'U': contu = contu + 1;
					      break;		
			}
			pos++;
			lletra = input.charAt(pos);
		}

		System.out.println("Heu introduit el caracter A " + conta + " vegades");
		System.out.println("Heu introduit el caracter E " + conte + " vegades");
		System.out.println("Heu introduit el caracter I " + conti + " vegades");
		System.out.println("Heu introduit el caracter O " + conto + " vegades");
		System.out.println("Heu introduit el caracter U " + contu + " vegades");


	}  //Fi dael maain
}

