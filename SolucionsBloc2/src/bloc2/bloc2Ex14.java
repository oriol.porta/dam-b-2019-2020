package bloc2;

//Ex 14. Algorisme que llegeix una seqüència de lletres (que formen una frase) fins arribar al punt. La sortida ens diu el número de vegades que llegeix la lletra A (tant majúscula com a minúscula).

import java.util.Scanner;  //Classe de Java que ens permet llegir el contingut d'entrada de dades

public class bloc2Ex14 {
	
	public static void main (String [] args) {
		Scanner reader = new Scanner(System.in);  //Creem un objecte que treballa sobre la consola (System.in)
		
		
		//char charAtAnyPos= input.charAt(pos);  // in pos you enter that index from where you want to get the char from	
		
		char lletra;
		int cont=0;
		int pos = 0;
	

		System.out.println("Escriu una frase acabada en punt ");
		String input = reader.nextLine();  //Llegeix la frase sencera i es guarda en input
		
		
		lletra = input.charAt(0);		//Llegeix un caracter de teclat
		
		while (lletra != '.')
		{
			if ((lletra == 'a') || (lletra == 'A'))
			{
				cont = cont + 1;
			}
			pos++;
			lletra = input.charAt(pos);
		}

		System.out.println("Heu introduit el caracter A " + cont + " vegades");


	}  //Fi dael maain
}

