package bloc2;

//Ex 2. Algorisme que calcula la suma i el producte dels 5 primers números naturals.



import java.util.Scanner;  //Classe de Java que ens permet llegir el contingut d'entrada de dades

public class bloc2Ex2 {
	
	public static void main (String [] args) {
		Scanner reader = new Scanner(System.in);  //Creem un objecte que treballa sobre la consola (System.in)
			
		int num = 1;
		int suma = 0;
		int prod = 1;
		
		//Obtenir números i tractar-los amb un bocle while
		
		while (num <= 5)
		{
			suma = suma + num;
			prod = prod * num;
			num++;
			
		}
		
		System.out.println("Producte dels números 1,2,3,4,5: " + prod);
		System.out.println("Suma dels números 1,2,3,4,5: " + suma);
		
	}  //Fi dael maain
}

