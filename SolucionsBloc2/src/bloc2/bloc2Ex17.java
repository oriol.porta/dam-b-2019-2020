package bloc2;

//Ex 17. Algorisme que llegeix una seqüència de 
//frases (un vers). El programa acaba quan li diem
// que no volem introduir més frases. La sortida ens diu per a cada frase quants caràcters té. En acabar ens diu quantes frases hem introduït.

import java.util.Scanner;  //Classe de Java que ens permet llegir el contingut d'entrada de dades

public class bloc2Ex17 {
	
	public static void main (String [] args) {
		Scanner reader = new Scanner(System.in);  //Creem un objecte que treballa sobre la consola (System.in)
		
		//char charAtAnyPos= input.charAt(pos);  // in pos you enter that index from where you want to get the char from	
		
		char lletra;
		int contlletra, contfrase;
		char resp='s';
		int pos = 0;
		String input;

		contfrase = 0;

		while (resp == 's' || resp == 'S')
		{
			pos=0;

			System.out.println("Escriu una frase acabada en punt ");
			input = reader.nextLine(); 
			
			lletra = input.charAt(0);
			while (lletra!='.')
			{
				pos++;
				lletra = input.charAt(pos);
			}
			System.out.println("Aquesta frase te " + pos + " caracters.");
			contfrase++;
			System.out.println("Voleu introduir mes frases (s/n)");
			
			resp = reader.next().charAt(0);
			reader.nextLine();    //per netejar el buffer d'entrada del \n que ha quedat darrere de llegir un número     
		} 

		System.out.println("Heu introduit " + contfrase + " frases." );	

	}  //Fi dael maain
}

