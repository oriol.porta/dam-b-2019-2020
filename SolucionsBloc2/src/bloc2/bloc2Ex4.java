package bloc2;

//Ex 4. Algorisme que llegeix una seqüència de notes enteres (0..10) fins que llegeix el -1 i ens diu si alguna de les notes era un 10.


import java.util.Scanner;  //Classe de Java que ens permet llegir el contingut d'entrada de dades

public class bloc2Ex4 {
	
	public static void main (String [] args) {
		Scanner reader = new Scanner(System.in);  //Creem un objecte que treballa sobre la consola (System.in)
			
		int nota = 0;
		int cont = 0;
		
		boolean fin = false;
		
		//Obtenir la primera nota a tractar fora del bucle 
		
		while (!fin) {
			System.out.print("Introdueix una nota: ");
			
			try {
				nota = reader.nextInt();
				
				switch(nota) {
					case -1: fin = true;
					break;
					case 10: cont++;
					break;
					case 0:
					case 1:
					case 2:
					case 3:
					case 4:
					case 5:
					case 6:
					case 7:
					case 8:
					case 9: break;
					default: System.out.println("Atenció! Únicament es permet insertar núotes entre 0 i 10. ");
					
				}
						
				
			} catch (Exception e){
				System.out.println("Atenció! Únicament es permet insertar núotes entre 0 i 10. ");
				reader.nextLine();    //per netejar el buffer d'entrada del \n que ha quedat darrere de llegir un número
			}
		}
	
		if (cont > 0)
		
			System.out.println("S'han contat :" + cont + " deus");
		
		else 
			System.out.println("No s'ha trobat cap 10");
		
		
	}  //Fi dael maain
}

