package bloc2;

import java.util.Scanner;

/*Institut Sabadell
 *CFGS DAM M03 UF1
 *Autor: Isaac Garc�a
 *Bloc 2 Exercici 4.1
 *4.8.	Algorisme que llegeix un n�mero enter positiu 
 i ens calcula el factorial (n!).
 */

public class Activitat4_008 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n=1;
		long factorial=1;
		int x=0;
		boolean z= true;
		
		System.out.print("Introdueix un n�mero enter i et mostrar� el seu factorial (n!): ");
		while (!z) {
			
				//comprovaci� del tipus de dada introdu�da
				try {
					n = sc.nextInt();
					z=false;
				} catch (Exception e){
					System.out.println("Atenci�! Unicament es permet insertar n�meros enters. ");
					sc.nextLine();    //per netejar el buffer d'entrada del \n que ha quedat darrere de llegir un n�mero.
					z=true;
					//fins aqui	
				}
		}
		
		while (n > 0) {
				factorial=factorial*n;
				n--;
		}
			
		 
		System.out.println("El factorial de "+ x +" �s: " +factorial);
		sc.close();
	}

}
