package bloc2;

//Ex 18. Algorisme que llegeix una seqüència de 
//lletres (que formen una frase) fins arribar al punt. La sortida ens diu quantes paraules conté la frase introduïda.

import java.util.Scanner;  //Classe de Java que ens permet llegir el contingut d'entrada de dades

public class bloc2Ex18 {
	
	public static void main (String [] args) {
		Scanner reader = new Scanner(System.in);  //Creem un objecte que treballa sobre la consola (System.in)
		
		//char charAtAnyPos= input.charAt(pos);  // in pos you enter that index from where you want to get the char from	
		
		char lletra;
		int cont;
		int pos = 0;
		boolean paraula = false;

		String input;
		
		cont = 0;
		
		System.out.println("Escriu una frase acabada en punt ");
		input = reader.nextLine(); 
			
		lletra = input.charAt(pos);
		while (lletra!='.') {
			
			while (lletra ==' ' )
			{
				pos++;
				lletra = input.charAt(pos);
			}
			
			if (lletra != '.')
				cont = cont + 1;
				
			while (lletra != ' ' && lletra != '.')
			{
					pos++;
					lletra = input.charAt(pos);
			}
		}
			
		System.out.println("S'han introduit: " + cont + " paraules");
	 
	}  //Fi del maain
}

