package bloc2;

//Ex 3. Algorisme que llegeix un número enter N i calcula la seva taula de multiplicar.

import java.util.Scanner;  //Classe de Java que ens permet llegir el contingut d'entrada de dades

public class bloc2Ex3 {
	
	public static void main (String [] args) {
		Scanner reader = new Scanner(System.in);  //Creem un objecte que treballa sobre la consola (System.in)
			
		int num = 0;
		int cont = 0;
		int prod = 1;
		boolean correcte = false;
		
		//Obtenir el número per calcular la seva taula de multiplicar
		
		while (!correcte) {
			System.out.print("Introdueix un número: ");
			try {
				num = reader.nextInt();  
				correcte = true;
			} catch (Exception e){
				System.out.println("Atenció! Únicament es permet insertar números enters. ");
				reader.nextLine();    //per netejar el buffer d'entrada del \n que ha quedat darrere de llegir un número
			}
		}
	
		//Calculem la taula de multiplicar de num
		
		cont = 1;
		while (cont <= 10)
		{
			System.out.println(num + " * " + cont + " = " + num*cont);
			cont ++;		
		}
		
		
	}  //Fi dael maain
}

