package bloc2;

//Ex 7. Algorisme que llegeix una seqÃ¼Ã¨ncia de nÃºmeros enters fins a llegir un zero. Llavors ens diu quin Ã©s el nÃºmero mÃ©s gran.


import java.util.Scanner;  //Classe de Java que ens permet llegir el contingut d'entrada de dades

public class Exercici7V3{
	
	public static void main (String [] args) {
		Scanner reader = new Scanner(System.in);  //Creem un objecte que treballa sobre la consola (System.in)
			
		int cont = 0, num = 0;
		int gran = Integer.MIN_VALUE;
		boolean fi = false;
		
		while (!fi) {
			System.out.print("Introdueix un número: ");
			try {
					num = reader.nextInt();  
					gran = num;
					fi = true;
			} catch (Exception e){
					System.out.println("AtenciÃ³! Ãšnicament es permet insertar nÃºmeros enters. ");
					reader.nextLine();    //per netejar el buffer d'entrada del \n que ha quedat darrere de llegir un nÃºmero
			}
		}
		
		fi = false;
		while (!fi)
		{
			System.out.print("Introdueix un número: ");
			try {
				num = reader.nextInt();  
				if (num == 0) fi = true;
				else {
					if (num > gran) gran = num;
				}
			} catch (Exception e){
				System.out.println("AtenciÃ³! Ãšnicament es permet insertar nÃºmeros enters. ");
				reader.nextLine();    //per netejar el buffer d'entrada del \n que ha quedat darrere de llegir un nÃºmero
			}	
		}
		
		System.out.println("El nÃºmero mÃ©s gran Ã©s: " + gran);
		
				
	}  //Fi dael maain
}

