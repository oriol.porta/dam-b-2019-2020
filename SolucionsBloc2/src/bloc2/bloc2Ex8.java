package bloc2;

//Ex 8. Algorisme que llegeix un número enter positiu i ens calcula el factorial (N!).

import java.util.Scanner;  //Classe de Java que ens permet llegir el contingut d'entrada de dades

public class bloc2Ex8 {
	
	public static void main (String [] args) {
		Scanner reader = new Scanner(System.in);  //Creem un objecte que treballa sobre la consola (System.in)
			
		int num = 0; 
		long fact = 1;
		boolean fi = false;
		
		
		while (!fi)
		{
			System.out.print("Introdueix un número: ");
			try {
				num = reader.nextInt();  
				if (num >= 0) fi = true;
				else  System.out.println("Atenció! introdueix un número positiu");
			} catch (Exception e){
				System.out.println("Atenció! Únicament es permet insertar números enters. ");
				reader.nextLine();    //per netejar el buffer d'entrada del \n que ha quedat darrere de llegir un número
			}	
		}
		//El número llegit és correcte
		if (num > 1) {
			while (num > 0) {
				fact = fact * num;
				num --;
			}
		}
		else fact = 1;
			
		System.out.println("El factorial de " + num + " és: " + fact);
		
				
	}  //Fi dael maain
}

