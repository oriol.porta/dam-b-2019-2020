package bloc2;

import java.util.Scanner;

/*Algorisme que llegeix una seq��ncia de n�meros enters fins a llegir un zero. Llavors ens diu quin �s el n�mero 
 *m�s gran.
*/
public class Exercici7_2 {

	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		int numero = 1;
		int major = 0;

		while (numero != 0) {
			System.out.println("Introdueix n�mero: ");
			try {
				numero = reader.nextInt();
				if (numero > major) {
					major = numero;
				}

			} catch (Exception e) {
				System.err.println("!ERROR�El valor te que ser enter: ");
				reader.nextLine();
			}
		}

		System.out.print("El n�mero m�s gran �s el: " + major);

		reader.close();
	}
}