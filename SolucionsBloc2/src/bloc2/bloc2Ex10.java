package bloc2;

//Ex 10. Algorisme que llegeix un número enter X i un altre número enter n i ens calcula l’N-èsima potencia de X.

import java.util.Scanner;  //Classe de Java que ens permet llegir el contingut d'entrada de dades

public class bloc2Ex10 {
	
	public static void main (String [] args) {
		Scanner reader = new Scanner(System.in);  //Creem un objecte que treballa sobre la consola (System.in)
			
		int x = 0, n = 0; 
		float pot = 1.0f;
		int cont = 0;
		boolean fi = false, positiu;
		
		
		while (!fi)
		{
			System.out.print("Introdueix el valor de n (potència): ");
			try {
				n = reader.nextInt();  
				fi = true;
			} catch (Exception e){
				System.out.println("Atenció! Únicament es permet insertar números enters. ");
				reader.nextLine();    //per netejar el buffer d'entrada del \n que ha quedat darrere de llegir un número
			}	
		}
		
		fi = false;
		while (!fi)
		{
			System.out.print("Introdueix el valor de x (número elevat): ");
			try {
				x = reader.nextInt();  
				fi = true;
			} catch (Exception e){
				System.out.println("Atenció! Únicament es permet insertar números enters. ");
				reader.nextLine();    //per netejar el buffer d'entrada del \n que ha quedat darrere de llegir un número
			}	
		}
		
		//Els dos números llegits són correctes
		
		if (n == 0) pot = 1;
		else if (n > 0) { //la potencia és positiva
				cont = n;
				while (cont > 0) {
						pot = pot * x;
						cont --;
				}
			}
			else { //La potencia és negativa
				cont = -n;
				while (cont > 0) {
						pot = pot * 1/x;
						cont --;
				}
			}
			
		System.out.println(x + " elevat a " + n + " dona com resultat: " + pot);
		
				
	}  //Fi dael maain
}

