package bloc2;

import java.util.Scanner;

/*Institut Sabadell
 *CFGS DAM M03 UF1
 *Autor: Isaac García
 *Bloc 2 Activitat 4
 *4.13.	Algorisme que llegeix una seqüència de notes entre 0 i 10. La seqüència s’acaba amb el -1. En acabar ens mostra: 
 *la mitjana aritmètica de totes les notes introduïdes, quants han tret excel•lent, quants notable, quants bé, quants suficient, 
 *quants insuficient i quants molt deficient.
 */


public class bloc2Ex13V2 {
	public static final int EXIT= -1;
	public static void main(String[] args) {
		double avg=0;
		int a=0; int b=0; int c=0; 
		int d=0; int ee=0; int f=0;
		int n=0;
		int mitjana=0;
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Introdueix les teves notes i et diré quants excel·lents, "
				+ "notables, bé, suficients, insuficients i molt deficients has tret"
				+ "també et mostrare la mitjana");
		
		while (n!=EXIT) {
			try {
				System.out.print("Introdueix un número enter: ");
				n = sc.nextInt();
				if (n<0 || n>10) {
					System.out.println("No pots treure menys d'un zero o més d'un 10!");
				} 
				else if (n<=2) {
					f++;
					avg++;
					mitjana=mitjana+n;
				}
				else if (n<5) {
					ee++;
					avg++;
					mitjana=mitjana+n;
				}
				else if (n<6) {
					d++;
					avg++;
					mitjana=mitjana+n;
				}
				else if (n<7) {
					c++;
					avg++;
					mitjana=mitjana+n;
				}
				else if (n<9) {
					b++;
					avg++;
					mitjana=mitjana+n;
				}		
				else {
					a++;
					avg++;
					mitjana=mitjana+n;
				}	
			} catch (Exception e){
				System.out.println("Atenció! Unicament es permet insertar números enters. ");
				sc.nextLine();    //per netejar el buffer d'entrada del \n que ha quedat darrere de llegir un número.
				//fins aqui			
			}		
		}
		sc.close();
		System.out.println("Has tret "+a +" Excel·lents");
		System.out.println("Has tret "+b +" Notables");
		System.out.println("Has tret "+c +" Bé");
		System.out.println("Has tret "+d +" Suficients");
		System.out.println("Has tret "+ee +" Insuficients");
		System.out.println("Has tret "+f +" Molt deficients");
		System.out.println("Y la teva mitjana és: "+mitjana/avg);

	}

}
