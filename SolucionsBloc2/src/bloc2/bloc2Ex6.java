package bloc2;

//Ex 6. Algorisme que llegeix una seqüència de 10 números i ens diu quants són positius, quants són negatius i quants són zero.


import java.util.Scanner;  //Classe de Java que ens permet llegir el contingut d'entrada de dades

public class bloc2Ex6 {
	
	public static void main (String [] args) {
		Scanner reader = new Scanner(System.in);  //Creem un objecte que treballa sobre la consola (System.in)
			
		int cont = 0, num = 0;
		int sumapos, sumaneg, sumazero;
		
		sumapos = 0;
		sumaneg = 0;
		sumazero = 0;
		
		while (cont < 10)
		{
			System.out.print("Introdueix un número: ");
			try {
				num = reader.nextInt();  
				cont++;
				if (num > 0) sumapos++;
				if (num < 0) sumaneg++;
				if (num == 0) sumazero++;
			} catch (Exception e){
				System.out.println("Atenció! Únicament es permet insertar números enters. ");
				reader.nextLine();    //per netejar el buffer d'entrada del \n que ha quedat darrere de llegir un número
			}	
		}
		
		System.out.println("El total de números positus introduits és: " + sumapos);
		System.out.println("El total de números negatius és: " + sumaneg);
		System.out.println("El total de números zero és: " + sumazero);
		 
		
				
	}  //Fi dael maain
}

