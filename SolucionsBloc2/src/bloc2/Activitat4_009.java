package bloc2;

import java.util.Scanner;

/*Institut Sabadell
 *CFGS DAM M03 UF1
 *Autor: Isaac Garc�a
 *Bloc 2 Exercici 4.1
 *4..9.	Algorisme que llegeix dos n�meros enters positius
 i ens d�na el producte mitjan�ant sumes successives.
 */


public class Activitat4_009 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int a=0;
		int b=0;
		int resultat=0;
		int i=0;
		boolean z= true;
		boolean x= true;
		
		System.out.println("Introdueix 2 n�meros enters i et mostrar� el seu producte amb sumes: ");
		while (z==true || x==true) {
			
				try {
					System.out.print("Introdueix el primer n�mero: ");
					a = sc.nextInt();
					//resultat=a;
					z=false;
				} catch (Exception e){
					System.out.println("Atenci�! Unicament es permet insertar n�meros enters. ");
					sc.nextLine();    //per netejar el buffer d'entrada del \n que ha quedat darrere de llegir un n�mero.
					z=true;
					//fins aqui	
			}
			
				try {
					System.out.print("Introdueix el seg�n n�mero: ");
					b = sc.nextInt();
					x=false;
				} catch (Exception e){
					System.out.println("Atenci�! Unicament es permet insertar n�meros enters. ");
					sc.nextLine();    //per netejar el buffer d'entrada del \n que ha quedat darrere de llegir un n�mero.
					x=true;
					//fins aqui	
				}
			
		
		}
		
		sc.close();
		
		i = 0;
		resultat = 0;
		System.out.println("el producte amb sumes �s:");
		while (i<b) {
			resultat=resultat+a;
			System.out.print("+"+a);
			i++;
		} 
		System.out.println(" el resultat �s: " +resultat);
	} 

}
