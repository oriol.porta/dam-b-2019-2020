package bloc2;

//Ex 13. Algorisme que llegeix una seqüència de 
//notes entre 0 i 10. La seqüència s’acaba amb el -1
//. En acabar ens mostra: la mitjana aritmètica de 
//totes les notes introduïdes, quants han tret excel·lent, quants notable, quants bé, quants suficient, quants insuficient i quants molt deficient.

import java.util.Scanner;  //Classe de Java que ens permet llegir el contingut d'entrada de dades

public class bloc2Ex13 {
	
	public static void main (String [] args) {
		Scanner reader = new Scanner(System.in);  //Creem un objecte que treballa sobre la consola (System.in)
			
		int nota;
		float mitjana=0;
		int excelent=0 ,notable=0 ,be=0 ,sufi=0 ,insu=0, md=0;
		int cont=0;
		boolean fi = false;

		
		while (!fi)
		{
			System.out.print("Introdueix una nota: ");
			try {
				nota = reader.nextInt();  
				if (nota == -1) fi = true;
				else
				{
					switch (nota) {
						case 0:
						case 1:
						case 2:
						case 3: md++;
								
								break;
						case 4: insu++;
								
								break;
						case 5: sufi++;
								
								break;
						case 6: be++;
								
								break;
						case 7:
						case 8: notable++;
								
								break;
						case 9:
						case 10: excelent++;
								
								 break;
						default : System.out.println("Nota no Vàlida");
					}
					mitjana = mitjana + nota;
					cont++;
				}
				
			} catch (Exception e){
				System.out.println("Atenció! Únicament es permet insertar números enters. ");
				reader.nextLine();    //per netejar el buffer d'entrada del \n que ha quedat darrere de llegir un número
			}	
		}
		
		mitjana = mitjana / cont;
		//Mostrem el resultat final
		
		System.out.println("La nota mitja de totes les notes es:" + mitjana);
		System.out.println("Excel.lents: " + excelent);
		System.out.println("Notables: " + notable);
		System.out.println("Bé: " + be);
		System.out.println("Suficients: " + sufi);
		System.out.println("Insuficients: " + insu);
		System.out.println("Molt Deficients: " + md);
  
  
		
	

	}  //Fi dael maain
}

