package bloc2;

import java.util.Scanner;

/*Institut Sabadell
 *CFGS DAM M03 UF1
 *Autor: Isaac Garc�a
 *Bloc 2 Exercici 4.1
 *4.6. Algorisme que llegeix una seq��ncia de 10 n�meros
 i ens diu quants s�n positius, quants s�n negatius i 
 quants s�n zero.
 */

public class Activitat4_006 {
	public static final int MAX = 10;
	public static void main(String[] args) {
		int i=0;
		int num=0;
		int neg=0;
		int pos=0;
		int zero=0;
		boolean z = true;
		Scanner sc = new Scanner(System.in);
			
		while (i!=MAX) {
			System.out.print("Introdueix " +MAX +" n�meros enters i et dir� quants s�n negatius i quants positius: ");
			//comprovaci� del tipus de dada introdu�da
			try {
				num = sc.nextInt();
				if (num >0) { 
				pos++;
				} else if (num <0) {
					neg++;
				} else {
					zero++;
				} 
				i++;
			} catch (Exception e){
				System.out.println("Atenci�! Unicament es permet insertar n�meros enters. ");
				sc.nextLine();    //per netejar el buffer d'entrada del \n que ha quedat darrere de llegir un n�mero
				z=false;
			//fins aqui	
			}
			
		} 
		System.out.println("Has posat:\n"+ pos +
		" N�meros positius\n"  +neg +
		" Negatius\n" +zero +" Zeros" );
		sc.close();
	}
}

