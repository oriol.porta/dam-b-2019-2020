package bloc2;

//Ex 11. Algorisme que llegeix un número enter N i
// ens diu la llista de números que són divisors 
//d’aquest.

import java.util.Scanner;  //Classe de Java que ens permet llegir el contingut d'entrada de dades

public class bloc2Ex11 {
	
	public static void main (String [] args) {
		Scanner reader = new Scanner(System.in);  //Creem un objecte que treballa sobre la consola (System.in)
			
		int num = 0, divisor;
		boolean fi = false;
		
		
		while (!fi)
		{
			System.out.print("Introdueix el valor de n: ");
			try {
				num = reader.nextInt();  
				if (num < 0) num = -num;
				fi = true;
			} catch (Exception e){
				System.out.println("Atenció! Únicament es permet insertar números enters. ");
				reader.nextLine();    //per netejar el buffer d'entrada del \n que ha quedat darrere de llegir un número
			}	
		}
		
        divisor = 1;
        

		while (num/2 >= divisor)
		{
			 if (num % divisor == 0)
			 {
				System.out.print(divisor + " ");
			 }
			divisor=divisor+1;

		 }
		 System.out.println(num);

				
	}  //Fi dael maain
}

