import java.util.Scanner;

public class ValidacioV2 {

	public static void main (String [] args) {
		Scanner reader = new Scanner(System.in);
		
		int num = 0;
	    while (num != -1) {
	    	System.out.print("Introdueix un n�mero: ");
	    }
	        if (reader.hasNext()) {
	            if (reader.hasNextInt()) {
	                num = reader.nextInt();
	            } else {
	                reader.next();
	                System.out.println(" no �s un enter.\nSi us plau, posa un n�mero enter");
	            }
	        } else {
	            System.out.println("No hi ha res a llegir d'entrada");
	        }
	    } 
}
