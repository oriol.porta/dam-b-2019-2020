
//Exemple de fer servir el switch
//Llegeix un n�mero de mes i mostra el nom del m�s corresponent

import java.util.Scanner;  //Classe de Java que ens permet llegir el contingut d'entrada de dades

public class MesosAmbIf {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner reader = new Scanner(System.in);  //Creem un objecte que treballa sobre la consola (System.in)
		
		int mes = 0;
	
		System.out.println("Escriu un n�mero de mes ");
		
		mes = reader.nextInt(); 

		if (mes == 1) {
			System.out.println("Gener");
		}
		else {
			if (mes == 2) {
				System.out.println("Febrer");
			}
			else {
				if (mes == 3) {
					System.out.println("Mar�");
				}
				else {
					if (mes == 4) {
						System.out.println("Abril");
					}
					else {
						if (mes == 5) {
							System.out.println("Maig");
						}
						else {
							if (mes == 6) {
								System.out.println("Juny");
							}
							else {
								if (mes == 7) {
									System.out.println("Juliol");
								}
								else {
									if (mes == 8) {
										System.out.println("Agost");
									}
									else {
										if (mes == 9) {
											System.out.println("Setembre");
										}
										else {
											if (mes == 10) {
												System.out.println("Octubre");
											}
											else {
												if (mes == 11) {
													System.out.println("Novembre");
												}
												else {
													if (mes == 12) {
														System.out.println("Desembre");
													}
													else {
														System.out.println("Error, mes no v�lid");
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

}
