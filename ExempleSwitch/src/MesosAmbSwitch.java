import java.util.Scanner;

//Exemple de fer servir el switch
//Llegeix un n�mero de mes i mostra el nom del m�s corresponent
//Ara fent servir SWITCH per distinguir el que s'ha de fer cada mes

public class MesosAmbSwitch {

	public static void main(String[] args) {
	
		Scanner reader = new Scanner(System.in);  //Creem un objecte que treballa sobre la consola (System.in)
				
		int mes = 0;
			
		System.out.println("Escriu un n�mero de mes ");
			
		mes = reader.nextInt(); 

		switch (mes) {
					case 1: System.out.println("Gener");
							break;
					case 2: System.out.println("Febrer");
							break;
					case 3: System.out.println("Mar�");
							break;
					case 4: System.out.println("Abril");
							break;
					case 5: System.out.println("Maig");
							break;
					case 6: System.out.println("Juny");
							break;
					case 7: System.out.println("Juliol");
							break;
					case 8: System.out.println("Agost");
							break;
					case 9: System.out.println("Setembre");
							break;
					case 10: System.out.println("Octubre");
							break;
					case 11: System.out.println("Novembre");
							break;
					case 12: System.out.println("Desembre");
							break;
					default: System.out.println("Error, mes no v�lid");
				
		}
			
				

	}

}
