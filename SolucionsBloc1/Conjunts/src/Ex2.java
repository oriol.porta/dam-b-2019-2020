import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Ex2 {
	static Scanner sc=new Scanner(System.in);
	public static void main(String[] args) {
		System.out.println("Digues un n�mero:");
		int num1=sc.nextInt();
		System.out.println("Digues un altre n�mero:");
		int num2=sc.nextInt();
		Set<Integer> conjunt1=new HashSet<Integer>();
		Set<Integer> conjunt2=new HashSet<Integer>();
		Set<Integer> conjunt3=new HashSet<Integer>();
		
		for(int i=2;i<1000;i++) {
			if(num1%i==0 && num2%i==0) {
				conjunt1.add(i);
			}
		}
		for(int i=2;i<1000;i++) {
			if(num1%i==0 || num2%i==0) {
				conjunt2.add(i);
			}
		}
		for(int i=2;i<100;i++) {
			if(num1%i!=0 && num2%i!=0) {
				conjunt3.add(i);
			}
		}
		System.out.println("");
		for (Integer i: conjunt1) {
			System.out.println(i);
		}
		System.out.println("===============");
		for (Integer i: conjunt2) {
			System.out.println(i);
		}
		System.out.println("=================");
		for (Integer i: conjunt3) {
			System.out.println(i);
		}
	}

}
