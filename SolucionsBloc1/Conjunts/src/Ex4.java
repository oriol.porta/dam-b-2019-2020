import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Ex4 {
	static Scanner sc = new Scanner(System.in);
	public static void main(String[] args) {
		int opcio;
		Map<String, String> llista = new HashMap<String, String>();
		llista.put("Firefox", "3.5");
		llista.put("Premiere", "CS3");
		llista.put("After Effects", "CS3");
		llista.put("Photoshop", "CS4");
		llista.put("Gimp", "2.0.1");
		llista.put("Python", "2.6.2");
		llista.put("SnagIt", "7.0.1");
		
		
		do {
			opcio = menu();
			switch (opcio) {
			case 1: 
				System.out.println(llista+ "\n");
				break;
			case 2: 
				System.out.println("Digues el programa:");
				String programa = sc.nextLine();
				System.out.println(llista.get(programa)+ "\n");
				break;
			case 3: 
				System.out.println("Digues el programa:");
				String prog = sc.nextLine();
				System.out.println("Digues la versi�:");
				String vers = sc.nextLine();
				llista.put(prog, vers);
				break;
			}
		} while (opcio != 4);
		System.out.println("AD�U!!!");
	}
	static int menu() {
		int valor = -1;

		do {
			System.out.println(" 1.- Mostrar llista");
			System.out.println(" 2.- Consultar versi�");
			System.out.println(" 3.- Afegir un nou programa");
			System.out.println(" 4.- Sortir");

			System.out.print("\n\nTria la teva opci�: ");

			try {
				valor = Integer.parseInt(sc.nextLine());
			} catch (Exception e) {
				System.out.println("Si us plau, valors num�rics");
			}
		} while (valor == -1);
		return (valor);
	}
}
