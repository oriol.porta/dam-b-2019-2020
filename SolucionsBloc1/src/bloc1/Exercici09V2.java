package bloc1;

import java.util.Scanner;

/*
	Bloc 1 Exercici 9
	Enunciat: Algorisme que llegeix un n�mero i ens diu si �s m�ltiple de dos o de tres o de cap d�ells.
	Autor: Marc Pernias
 */

public class Exercici09V2 {

		public static void main(String[] args) {
			// TODO Auto-generated method stub
			
			Scanner reader = new Scanner(System.in);
			
			int num = 0;
			
			System.out.print("Afegeix un n�mero ");
			num = reader.nextInt();
			
			if(num % 2 == 0) {
				
				System.out.println("El n�mero "+num+" �s multiple de 2");
			}
			
			else  
				if (num % 3 == 0) {
				System.out.println("El n�mero "+num+" �s multiple de 3");
			}
				

			
			else {
				System.out.println("El n�mero "+num+" no �s multiple ni de 2, ni de 3");
			}
			
			reader.close();

		}

}
