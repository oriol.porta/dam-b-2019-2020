package bloc1;

import java.util.Scanner;

/*
 * Exercici 2:  Algorisme que llegeix un n�mero i compara si �s major que 10. 
 * Autora: Elena
 */

public class Exercici02 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int num;
		Scanner reader = new Scanner(System.in);
		
		System.out.print("Introdueix un n�mero enter: " );
		num = reader.nextInt();
		
		if (num > 10) {
			System.out.println("El n�mero introduit S� �s m�s gran que 10 " );
		}
		else {
			System.out.println("El n�mero introduit NO �s m�s gran que 10 " );
		}
		
		reader.close();
	}

}
