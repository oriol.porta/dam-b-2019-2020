package bloc1;

import java.util.Scanner;

/* Institut Sabadell
 * CFGS DAM M03 UF1
 * Bloc 1 Exercici 10
 * Descripcio: Algorisme que llegeix dos n�meros enters positius i diferents i ens diu si el major �s m�ltiple del menor, o el que �s al mateix, que el menor �s divisor del major. 
 */

public class Exercici10V2 {
	
	public static void main(String[] args) 
	{
		int a = 0;
		int b = 0;
		Scanner reader = new Scanner(System.in);
		
		System.out.println("Introdueix un n�mero:\n");
		a = reader.nextInt();
		System.out.println("\nIntrodueix un altre n�mero:\n");
		b = reader.nextInt();
		
		if ((a > 0 && a != b) && b > 0)
		{
			if (a > b)
			{
				if (a % b == 0)
				{
					System.out.println("\n" + a + " �s multiple de " + b);
				}
				else
				{
					System.out.println("\n" + a + " no �s multiple de " + b);
				}
			}
			else
			{
				if (b % a == 0)
				{
					System.out.println("\n" + b + " �s multiple de " + a);
				}
				else
				{
					System.out.println("\n" + b + " no �s multiple de " + a);
				}
			}
		}
		else 
		{
			System.out.println("\nIntrodeuix dos n�meros diferents i positius.");
		}
		reader.close();
	}
}
