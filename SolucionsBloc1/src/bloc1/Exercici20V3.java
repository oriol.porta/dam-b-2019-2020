package bloc1;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Scanner;

public class Exercici20V3 {

public static void main (String [] args) {
		
		Scanner reader = new Scanner(System.in);
		Calendar data = new GregorianCalendar();
		int any = data.get(Calendar.YEAR);
		int mes = data.get(Calendar.MONTH) + 1;
		int dia = data.get(Calendar.DAY_OF_MONTH);
		
	
		///Hem agafat el dia actual del sistema. Si �s l'usuari qui introdueix la data, 
		///cal validar que sigui correcte
		
		dia++;
		
		if ((mes == 2 && dia > 28) || 
			 ((mes == 1 || mes == 3 || mes == 5 || mes == 7 || mes == 8 || mes == 10) && dia > 31) ||
			 ((mes == 4 || mes == 6 || mes == 9 || mes == 11) && dia > 30)) {
					
						dia = 1;
						mes++;
		}
		else if (mes == 12 && dia > 31) {
							dia = 1;
							mes = 1;
							any++;
			  }
					
		
		System.out.println ("Dema es: " + dia + "/" + mes + "/" + any);

		reader.close();
		
	}
	
	
	
}
