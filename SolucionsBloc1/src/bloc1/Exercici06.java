package bloc1;

import java.util.Scanner;

	/* Institut Sabadell
	 * CFGS DAM M03 UF1
	 * Bloc 1 Exercici 6
	 * Descripcio: Algorisme que llegeix 3 n�meros i els mostra ordenats de major a menor. 
	 * Autor: Elena Del Pozo
	 */

	public class Exercici06 {
		
		public static void main(String[] args) 
		{
			int a = 0;
			int b = 0;
			int c = 0;
			Scanner reader = new Scanner(System.in);
			
			System.out.println("Introdueix un n�mero:\n");
			a = reader.nextInt();
			System.out.println("\nIntrodueix un altre n�mero:\n");
			b = reader.nextInt();
			System.out.println("\nIntrodueix l'ultim n�mero:\n");
			c = reader.nextInt();
			
			if (a > b)
			{
				if (b > c)
				{
					System.out.println("\n" + a + " > " + b + " > " + c);
				}
				else 
				{
					if (a > c) {
						System.out.println("\n" + a + " > " + c + " > " + b);
					}
					else {
						System.out.println("\n" + c + " > " + a + " > " + b);
					}
				}
			}
			else { //b > a) 
	                if (a > c)
	                {
	                    System.out.println("\n" + b + " > " + a + " > " + c);
	                }
	                else 
	                {	if (b > c) {
							System.out.println("\n" + b + " > " + c + " > " + a);
	                }
	            
						else {
							System.out.println("\n" + c + " > " + b + " > " + a);
						}
	                
	            }
			}
			reader.close();
		}
	}

