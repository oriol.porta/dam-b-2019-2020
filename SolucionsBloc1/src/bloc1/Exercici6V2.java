package bloc1;

/*Institut Sabadell. 
CFGS DAM  M03 UF1
Bloc 1 Exercici 6
Descripci�: Algorisme que llegeix 3 n�meros per teclat i els escriu de m�s gran a m�s petit
*/
import java.util.Scanner;  //Classe de Java que ens permet llegir el contingut d'entrada de dades

public class Exercici6V2 {
		
	public static void main (String [] args) {
		Scanner reader = new Scanner(System.in);  //Creem un objecte que treballa sobre la consola (System.in)
		int num1 = 0;	//Variable on guargar� el n�mero llegit per teclat
		int num2 = 0;
		int num3 = 0;
			
		//Obtenir dades ...
		System.out.print("Introdueix un n�mero enter: ");
		num1 = reader.nextInt();  //Llegeix un n�mero de la consola i el guarda a la variable num
		System.out.print("Introdueix un n�mero enter: ");
		num2 = reader.nextInt();  //Llegeix un n�mero de la consola i el guarda a la variable num
		System.out.print("Introdueix un n�mero enter: ");
		num3 = reader.nextInt();  //Llegeix un n�mero de la consola i el guarda a la variable num

		//Tractament de les dades i resultats
					
		if (num1 >= num2 && num2 >= num3)  
			System.out.println("De gran a petit: " + num1 + " " + num2 + " " + num3);				
	
		else if (num1 >= num3 && num3 >= num2)
			System.out.println("De gran a petit: " + num1 + " " + num3 + " " + num2);	
			
		else if (num2 >= num1 && num1 >= num3)
			System.out.println("De gran a petit: " + num2 + " " + num1 + " " + num3);	
				 
		else if (num2 >= num3 && num3 >= num1)
			System.out.println("De gran a petit: " + num2 + " " + num3 + " " + num1);	
				 		 
		else if (num3 >= num1 && num1 >= num2)
			System.out.println("De gran a petit: " + num3 + " " + num1 + " " + num2);	
				 
		else 
			System.out.println("De gran a petit: " + num3 + " " + num2 + " " + num1);	
		
		reader.close();
	}
}
