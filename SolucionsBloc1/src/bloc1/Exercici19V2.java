package bloc1;

import java.util.Scanner;

public class Exercici19V2 {
 
	public static void main (String [] args) {
		Scanner reader = new Scanner(System.in);  //Creem un objecte que treballa sobre la consola (System.in)
		//java.util.Date fechaActual = new java.util.Date(); //Fecha actual del sistema
		
		int da = 0, ma = 0, aa;	
		
		boolean error = false;
		
		//Obtenir dades ...
		
		System.out.println("Dia : \n");
		da = reader.nextInt();
		System.out.println("\nMes: \n");
		ma = reader.nextInt();
		System.out.println("\nAny: \n");
		aa = reader.nextInt();


        //Validaci� de la data de naixament de l'usuari
		if (da < 1 || da > 31) {
            error = true;
            System.out.println("Dia no correcte");
		}

		if (error == false){
            switch(ma){
                    case 1:
                    case 3:
                    case 5:
                    case 7:
                    case 8:
                    case 10:
                    case 12: break;
                    case 2:  if (da > 28) {
                                error = true;
                                System.out.println("Aquest mes no pot tenir m�s de 28 dies");
                             }
                             break;
                    case 4:
                    case 6:
                    case 9:
                    case 11: if (da > 30) {
                                error = true;
                                System.out.println("Aquest mes no pot tenir m�s de 30 dies");
                             }
                             break;
                    default: error = true;
                             System.out.println("No has posat un n�mero de mes v�lid");
                }

        }
		if (error == false && (aa < 1900)) {
			error = true;
            System.out.println("Any incorrecte");
		}
        //Fi de la validaci� de la data
		
		
		if (error == false)    
		 {
			switch (ma) {
				case 1: if (da <= 21)
							System.out.println("Ets Capricorni (23-12 / 21-1)");
						else
							System.out.println("Ets Acuari (22-1 / 21-2)");
						break;
						
				case 2: if (da <= 21)
							System.out.println("Ets Acuari (22-1 / 21-2)");
						else
							System.out.println("Ets Peixos (22-2 / 20-3)");
						break;
						
				case 3:  if (da>=21)
							System.out.println("Ets Aries (21-3 / 20-4)");
						 else
							 System.out.println("Ets Peixos (22-2 / 20-3)");
						break;
						
				case 4: if (da <= 20)
							System.out.println("Ets Aries (21-3 / 20-4)");
						else
							System.out.println("Ets Taure (21-4 / 20-5)");
						break;
						
				case 5: if (da <= 21)
							System.out.println("Ets Taure (21-4 / 20-5)");
						else
							System.out.println("Ets Bessons (21-5 / 21-6)");
						break;
						
				case 6: if (da <= 21)
							System.out.println("Ets Bessons (21-5 / 21-6)");
						else
							System.out.println("Ets Cranc (22-6 / 22-7)");
						break;
						
				case 7: if (da <= 22)
							System.out.println("Ets Cranc (22-6 / 22-7)");
						else
							System.out.println("Ets Lleo (23-7 / 22-8)");
						break;
						
				case 8: if (da <= 22)
							System.out.println("Ets Lleo (23-7 / 22-8)");
						else
							System.out.println("Ets Verge (23-8 / 21-9)");
						break;
						
				case 9: if (da <= 23)
							System.out.println("Ets Verge (23-8 / 21-9)");
						else
							System.out.println("Ets Balanca (22-9 / 22-10)");
						break;
						
				case 10: if (da <= 22)
							System.out.println("Ets Balanca (22-9 / 22-10)");
						else
							System.out.println("Ets Escorpi (23-10 / 21-11)");
						break;
						
				case 11: if (da <= 21)
							System.out.println("Ets Escorpi (23-10 / 21-11)");
						 else
							System.out.println("Ets Sagitari (22-11 / 22-12)");
						 break;
						
				case 12: if (da <= 22)
							System.out.println("Ets Sagitari (22-11 / 22-12)");
						 else
							System.out.println("Ets Capricorni (23-12 / 21-1)");
						break;
				
			}
			
		 }	
		reader.close();
	}  //Fi dael maain
	
}
