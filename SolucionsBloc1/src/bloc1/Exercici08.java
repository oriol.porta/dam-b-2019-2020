package bloc1;
//Ex 8 - 1.8.- Algorisme i/o programa que calcula la superf�cie d�un rectangle. El programa llegeix d�entrada la grand�ria dels costats.

import java.util.Scanner;  //Classe de Java que ens permet llegir el contingut d'entrada de dades

public class Exercici08 {
	
	public static void main (String [] args) {
		Scanner reader = new Scanner(System.in);  //Creem un objecte que treballa sobre la consola (System.in)
		int l1 = 0;	//Variable on guargar� el n�mero llegit per teclat
		int l2 = 0;
		int res;
		
		//Obtenir dades ...
		System.out.print("Introdueix la grand�ria d'un costat del rectangle: ");
		l1 = reader.nextInt();  //Llegeix un n�mero de la consola i el guarda a la variable num
		System.out.print("Introdueix la grand�ria del segon costat: ");
		l2 = reader.nextInt();  //Llegeix un n�mero de la consola i el guarda a la variable num
		
		if (l1 > 0 && l2 > 0) {
			//Tractament de les dades i resultats
			res = l1 * l2;	
			System.out.println("L'�rea del rectangle �s: " + res);	
		}
		else {
			System.out.println("Error, els costats no poden ser zero");
		}
		reader.close();
	}  //Fi del main
}