package bloc1;
import java.util.Scanner;  //Classe de Java que ens permet llegir el contingut d'entrada de dades

/*Institut Sabadell. 
CFGS DAM  M03 UF1
Bloc 1 Exercici 7 
Descripci�: Algorisme que llegeix dos n�meros per teclat, i fa operacions de suma, resta, multiplicaci� i divisi�
*/
public class Exercici07 {

	public static void main (String [] args) {
		Scanner reader = new Scanner(System.in);  //Creem un objecte que treballa sobre la consola (System.in)
		double num1 = 0;	//Variable on guargar� el n�mero llegit per teclat
		double num2 = 0;
		double res;
			
		//Obtenir dades ...
		System.out.print("Introdueix un n�mero enter: ");
		num1 = reader.nextDouble();  //Llegeix un n�mero de la consola i el guarda a la variable num
		System.out.print("Introdueix un n�mero enter: ");
		num2 = reader.nextDouble();  //Llegeix un n�mero de la consola i el guarda a la variable num
			
		//Tractament de les dades i resultats
				
		res = num1 + num2;	
		System.out.println(num1 + " + " + num2 + " = " + res);	
				
		res = num1 - num2;	
		System.out.println(num1 + " - " + num2 + " = " + res);	
				
		res = num1 * num2;	
		System.out.println(num1 + " * " + num2 + " = " + res);	
		
		if (num2 != 0) {
			res = num1 / num2;
			System.out.println(num1 + " / " + num2 + " = " + res);
		}
		else
			System.out.println("No es pot dividir entre 0");
		
		reader.close();	
	}

}