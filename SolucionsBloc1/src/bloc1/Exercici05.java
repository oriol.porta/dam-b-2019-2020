package bloc1;
/* Institut Sabadell.
CFGS DAM M03 UF1
Bloc 1 Ejercicio 5 1.5.- Algorisme que llegeix 3 n�meros 
i digui quin �s el major. 
Descripcio: Enuncitat de exercici
Autor: David Ruiz
*/

import java.util.Scanner;

public class Exercici05 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int num1;
		int num2;
		int num3;
		Scanner sc = new Scanner(System.in);
		System.out.print("Introdueix el primer numero: ");
		num1=sc.nextInt();
		System.out.println("Introdueix el segon numero: ");
		num2=sc.nextInt();
		System.out.println("Introdueix el tercer numero: ");
		num3=sc.nextInt();
		
		if (num1>num2 && num1>num3) {
			System.out.println("El major �s "+num1);
		
		}
		else {if (num2>num1 && num2>num3 ) {
				System.out.println("El major �s "+num2);
			}
			else {	
					System.out.println("El major �s "+num3);
			}
			System.out.println("Adeu...");
			
		
		}
		sc.close();
	}
}
