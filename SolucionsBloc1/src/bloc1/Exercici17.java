package bloc1;

/*
 * //Enota 1.17.- Algorisme que llegeix el preu final pagat per un producte i
 el seu preu de tarifa i ens mostra el percentatge de descompte que li hem aplicat. 
 */

import java.util.Scanner;  //Classe de Java que ens permet llegir el contingut d'entrada de dades


public class Exercici17 {

	public static void main (String [] args) {
		Scanner reader = new Scanner(System.in);  //Creem un objecte que treballa sobre la consola (System.in)
		
		float pf, pt, desc;
		
		//Obtenir dades ...
		System.out.print("Introdueix el preu final pagat: ");
		pf = reader.nextFloat(); 
		System.out.print("Introdueix el preu de tarifa: ");
		pt = reader.nextFloat(); 
		
		desc = (100 - (pf * 100 / pt));
		
		System.out.println("El descuento ha sido de " + desc + "%");

		System.out.printf("El descuento ha sido de %.2f%%" , (100 - (pf * 100 / pt)) );
		
		reader.close(); 
	}  //Fi del main
}
