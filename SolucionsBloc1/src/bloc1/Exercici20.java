package bloc1;
/* 
 * 1.20.- Algorisme que llegeix una data d�entrada expressada en dia (1 a 31), mes
 *  (1 a 12) i any (amb n�mero) i ens diu la data que hi serem al dia seg�ent.
 *   Es suposa que febrer t� sempre 28 dies. 
 */

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Scanner;  //Classe de Java que ens permet llegir el contingut d'entrada de dades

public class Exercici20 {

	public static void main (String [] args) {
		
		Scanner reader = new Scanner(System.in);
		Calendar data = new GregorianCalendar();
		int any = data.get(Calendar.YEAR);
		int mes = data.get(Calendar.MONTH) + 1;
		int dia = data.get(Calendar.DAY_OF_MONTH);
		int diaMaxim;
	
		///Hem agafat el dia actual del sistema. Si �s l'usuari qui introdueix la data, 
		///cal validar que sigui correcte
		
		if (mes == 2)
			diaMaxim=28;
		  else 	{
				if (mes == 4 || mes == 6 || mes == 9 || mes == 11)
					diaMaxim=30;
				else
					diaMaxim=31;
		   }

		   if (dia == diaMaxim) {
				if (mes == 12) {
					any++;
					mes = 1;
					dia = 1;
				}
				else {
					mes++;
					dia = 1;
				}
				//mes =(mes % 12) + 1;
			}
		   else dia ++;
		   //dia = (dia % diaMaxim)+1;

			System.out.println ("Dema es: " + dia + "/" + mes + "/" + any);

			reader.close();
		
	}
	
}
