package bloc1;

/* Ex 1.19- Algorisme i/o programa que llegint una data de naixement ens 
 * doni l�hor�scop corresponent.
 */


import java.util.Scanner;  //Classe de Java que ens permet llegir el contingut d'entrada de dades

public class Exercici19 {

	public static void main (String [] args) {
		Scanner reader = new Scanner(System.in);  //Creem un objecte que treballa sobre la consola (System.in)
		//java.util.Date fechaActual = new java.util.Date(); //Fecha actual del sistema
		
		int da = 0, ma = 0, aa;	
		
		boolean error = false;
		
		//Obtenir dades ...
		
		System.out.println("Dia : \n");
		da = reader.nextInt();
		System.out.println("\nMes: \n");
		ma = reader.nextInt();
		System.out.println("\nAny: \n");
		aa = reader.nextInt();


        //Validaci� de la data de naixament de l'usuari
		if (da < 1 || da > 31) {
            error = true;
            System.out.println("Dia no correcte");
		}

		if (error == false){
            switch(ma){
		            case 4:
		            case 6:
		            case 9:
		            case 11: if (da > 30) {
		                        error = true;
		                        System.out.println("Aquest mes no pot tenir m�s de 30 dies");
		                     }
		                     break;
                    case 1:
                    case 3:
                    case 5:
                    case 7:
                    case 8:
                    case 10:
                    case 12: break;
                    case 2:  if (da > 28) {
                                error = true;
                                System.out.println("Aquest mes no pot tenir m�s de 28 dies");
                             }
                             break;
                    
                    default: error = true;
                             System.out.println("No has posat un n�mero de mes v�lid");
                }

        }
		if (error == false && (aa < 1900)) {
			error = true;
            System.out.println("Any incorrecte");
		}
        //Fi de la validaci� de la data
		
		
		if (error == false)   //if (!error)
		 {
			 if ((ma==3 && da>=21) || (ma==4 && da<=20))
				System.out.println("Ets Aries (21-3 / 20-4)");
			else
			{
				if ((ma==4 && da>=21)||(ma==5 && da<=20))
					System.out.println("Ets Taure (21-4 / 20-5)");
				else
				{
					if ((ma==5 && da>=21)||(ma==6 && da<=21))
						System.out.println("Ets Bessons (21-5 / 21-6)");
					else
                  {
                    if ((ma==6 && da>=22) || (ma==7 && da<=22))
							System.out.println("Ets Cranc (22-6 / 22-7)");
                    else
                        {
                            if ((ma==7 && da>=23) || (ma==8 && da<=22))
									System.out.println("Ets Lleo (23-7 / 22-8)");
                            else
                               {
                                if ((ma==8 && da>=23) ||(ma==9 && da<=21))
										System.out.println("Ets Verge (23-8 / 21-9)");
                                else
                                        {
                                        if ((ma==9 && da>=22)||(ma==10 && da<=22))
                                        System.out.println("Ets Balanca (22-9 / 22-10)");
                                        else
                                                {
                                                if ((ma==10 && da>=23)||(ma==11 && da<=21))
														System.out.println("Ets Escorpi (23-10 / 21-11)");
                                                else
                                                        {
                                                        if ((ma==11 && da>=22)||(ma==12 && da<=22))
																System.out.println("Ets Sagitari (22-11 / 22-12)");
                                                        else
                                                                {
                                                                if ((ma==12 && da>=23)||(ma==1 && da<=21))
																		System.out.println("Ets Capricorni (23-12 / 21-1)");
                                                                else

                                                                        {
                                                                        if ((ma==1 && da>=22)||(ma==2 && da<=21))
																				System.out.println("Ets Acuari (22-1 / 21-2)");
                                                                        else

                                                                                {
                                                                                if ((ma==2 && da>=22)||(ma==3 && da<=20))
																						System.out.println("Ets Peixos (22-2 / 20-3)");


										}}}}}}}}}}}


		 }
		reader.close();
	}  //Fi dael maain
	
}
