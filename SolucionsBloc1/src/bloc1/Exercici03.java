package bloc1;
/* Institut Sabadell.
CFGS DAM M03 UF1
Bloc 1 Ejercicio 3 Algorisme que llegeix dos n�meros i mostri
 com resultat qui �s el m�s gran.
Descripcio: Enuncitat de exercici
Autor: David Ruiz
*/

import java.util.Scanner;

public class Exercici03 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int num;
		int num1;
		Scanner sc = new Scanner(System.in);
		System.out.print("Introdueix el primer numero: ");
		num=sc.nextInt();
		System.out.println("Introdueix el segon numero: ");
		num1=sc.nextInt();
		
		if (num>num1) {
			System.out.println("Es mayor el primer numero");
		} else {
			System.out.println("Es mayor el segon numero");
		}
			
		sc.close();
	}
}
