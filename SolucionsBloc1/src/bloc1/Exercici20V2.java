package bloc1;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Scanner;

public class Exercici20V2 {

	public static void main (String [] args) {
		
		Scanner reader = new Scanner(System.in);
		Calendar data = new GregorianCalendar();
		int any = data.get(Calendar.YEAR);
		int mes = data.get(Calendar.MONTH) + 1;
		int dia = data.get(Calendar.DAY_OF_MONTH);
		
	
		///Hem agafat el dia actual del sistema. Si �s l'usuari qui introdueix la data, 
		///cal validar que sigui correcte
		
		dia++;
		
		switch (mes) {
			case 2: if (dia > 28) {
						dia = 1;
						mes++;
					}
					break;
			case 1: 
			case 3:
			case 5:
			case 7:
			case 8:
			case 10:	if (dia > 31) {
							dia = 1;
							mes++;
						}
						break;
			case 12: if (dia > 31) {
							dia = 1;
							mes = 1;
							any++;
					 }
					 break;
			case 4:
			case 6:
			case 9:
			case 11: if (dia > 30) {
						dia = 1;
						mes++;
					}
					break;
		}
		
		
		System.out.println ("Dema es: " + dia + "/" + mes + "/" + any);

		reader.close();
		
	}
	
	
	
}
