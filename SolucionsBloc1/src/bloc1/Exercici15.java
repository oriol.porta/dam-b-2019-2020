package bloc1;

import java.util.Scanner;

/* 1.15.- Algorisme que llegeix dos n�meros en dos variables i 
 * intercanvia el seu valor. 
 */


public class Exercici15 {

	public static void main (String [] args) {
		Scanner reader = new Scanner(System.in);  //Creem un objecte que treballa sobre la consola (System.in)
		
		int v1, v2, aux;
		//Obtenir dades ...
		System.out.print("Introdueix el primer valor: ");
		v1 = reader.nextInt(); 
		System.out.print("Introdueix el segon valor: ");
		v2 = reader.nextInt();  	
		
		//Opci� 1: fent servir una variable auxiliar
		aux = v1;
		v1 = v2;
		v2 = aux;
		
		//Opci� 2 - �nicament si treballem amb nombres enters...
		v1 = v1 + v2;
		v2 = v1 - v2;
		v1 = v1 - v2;
			 
		System.out.println("El valor actual de la primera variable es: " + v1);
		System.out.println("El valor actual de la segunda variable es: " + v2);
		reader.close();
	}  //Fi del main
	
}
