package bloc1;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Scanner;

/* Institut Sabadell
 * CFGS DAM M03 UF1
 * Bloc 1 Exercici 12
 * Descripcio: Algorisme que llegint una data de naixement i ens digui si pot votar (t� 18 anys o m�s).
 * Farem servir la data actual del sistema, en lloc de llegir-la d'entrada. Aix� ens estalviem llegir i validar aquestes tres dades
 */

public class Exercici12V2 {

	public static void main(String[] args)
	{
		Scanner reader = new Scanner(System.in);
		Calendar data = new GregorianCalendar();
		int anyActual = data.get(Calendar.YEAR);
		int mesActual = data.get(Calendar.MONTH) + 1;
		int diaActual = data.get(Calendar.DAY_OF_MONTH);
		
		int anyUsuari = 0;
		int mesUsuari = 0;
		int diaUsuari = 0;
		
		boolean correcte = true;   //Ens serveix de centinela per validar les dades

		System.out.println("Introdueix el teu dia de naixement: \n");
		diaUsuari = reader.nextInt();
		System.out.println("\nIntrodueix el teu mes de naixement: \n");
		mesUsuari = reader.nextInt();
		System.out.println("\nIntrodueix el teu any de naixement: \n");
		anyUsuari = reader.nextInt();


        //Validaci� de la data de naixament de l'usuari
		if (diaUsuari < 1 || diaUsuari > 31) {
            correcte = false;
            System.out.println("Dia no correcte");
		}

	//Validaci� del mes i dia amb if
/// if (mesUsuari >= 1 && mesUsuari <= 12) {
///     if (mesUsuari ==  2 && diaUsuari > 28) {
///             correcte = false;
///             System.out.println("Aquest mes no pot tenir m�s de 28 dies");
///     }
///     else {
///				if ((mesUsuari ==  4 || mesUsuari ==  6 || mesUsuari ==  9 || mesUsuari ==  11) && diaUsuari > 30) {
///                  correcte = false;
///                  System.out.println("Aquest mes no pot tenir m�s de 30 dies");
///         	} 
///		}
/// }
/// else {
///     correcte = false;
///     System.out.println("No has posat un n�mero de mes v�lid");
/// }
		
	//Validaci� del mes i dia amb switch
        
	if (correcte == true){
            switch(mesUsuari){
                    case 1:
                    case 3:
                    case 5:
                    case 7:
                    case 8:
                    case 10:
                    case 12: break;
                    case 2:  if (diaUsuari > 28) {
                                correcte = false;
                                System.out.println("Aquest mes no pot tenir m�s de 28 dies");
                             }
                             break;
                    case 4:
                    case 6:
                    case 9:
                    case 11: if (diaUsuari > 30) {
                                correcte = false;
                                System.out.println("Aquest mes no pot tenir m�s de 30 dies");
                             }
                             break;
                    default: correcte = false;
                             System.out.println("No has posat un n�mero de mes v�lid");
                }

        }
		if (correcte == true && (anyUsuari < 1900 || anyUsuari > anyActual)) {
            correcte = false;
            System.out.println("Any incorrecte");
		}
        //Fi de la validaci� de la data

		//Si el centinela no s'ha posat a false en aquest punt �s perqu� la data �s correcta
		if (correcte == true) {
			System.out.println("\n" + "Avui �s " + diaActual + "/" + mesActual + "/" + anyActual + "\n");
			if ((anyActual - anyUsuari > 18) || 
			    (anyActual - anyUsuari == 18 && mesUsuari <= mesActual && diaUsuari <= diaActual))	{
					System.out.println("\n" + "Vas neixer el " + diaUsuari + "/" + mesUsuari + "/" + anyUsuari + " aix� que ets major d'edat.");
			}
			else {
				   System.out.println("\n" + "Vas neixer el " + diaUsuari + "/" + mesUsuari + "/" + anyUsuari + " aix� que no ets major d'edat.");
			}
		}
				
		else
		{
			System.out.println("\nIntrodueix una data valida.");
		}
		reader.close();
	}
}
