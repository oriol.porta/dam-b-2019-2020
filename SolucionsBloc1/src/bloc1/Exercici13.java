package bloc1;

/*IES Sabadell.
CFGS DAM M03 UF1
Bloc 1 Exercici 13
Descripcio: Algorisme que llegeix 3 valors corresponents als coeficients A, B, i C
d�una equaci� de segon grau i ens diu la soluci� aplicant la f�rmula de resoluci�.
*/


import java.util.Scanner;

public class Exercici13 {

    public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner reader = new Scanner(System.in);
		int a = 0;
		int b = 0;
		int c = 0;
		double x1, x2, arrel;

		System.out.print("Introdueix un numero per al valor de 'a': ");
		a = reader.nextInt();

		System.out.print("Introdueix un numero per al valor de 'b': ");
		b = reader.nextInt();

		System.out.print("Introdueix un numero per al valor de 'c': ");
		c = reader.nextInt();

		reader.close();
        arrel = ((b*b)-(4*a*c));

        if (a != 0 && arrel > 0) {
            arrel = Math.sqrt(arrel);   //La classe Math disposa de m�todes amb funcions matem�tiques, com l'arrel: sqrt
            x1 = (-b+ arrel)/(2*a);
            x2 = (-b- arrel)/(2*a);
            System.out.println("El resultat de x1 �s: "+x1);
            System.out.println("El resultat de x2 �s: "+x2);
        }

        else {
            System.out.println("L'equaci� no t� soluci�");
        }


	}

}
