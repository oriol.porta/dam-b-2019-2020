package bloc1;

import java.util.Scanner;

/*
 * Exercici1: Algorisme que llegeix un n�mero per teclat, el multiplica per 3 
 * i mostra el resultat  
 * Autora: Elena
 */

public class Exercici01 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Declaraci� de variables
		int num;
		int res;
		Scanner reader = new Scanner(System.in);
		
		System.out.print("Introdueix un n�mero enter: " );
		num = reader.nextInt();
		
		res = num * 3;
		
		System.out.println("El resultat de " + num + " * 3  = " + res);
		
		reader.close();
	}

}
